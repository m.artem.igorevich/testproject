<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if ($this->startResultCache()) {
    $userID = $USER->getID(); //Получаем ID текущего авторизованного пользователя
    $activeType = $arParams['ACTIVE']; //Получаем вид отображения адресов (все или активные)
    $hlbl = 4; //ID HL-блока

    $arResult['TABLE'] = $this->getTable($userID, $activeType, $hlbl); //Формируем $arResult

    $this->IncludeComponentTemplate();
}