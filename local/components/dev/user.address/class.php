<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;

Loader::includeModule("highloadblock");

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Entity;

class HLAddress extends CBitrixComponent
{
    /**
     * @param $userID //iduser
     * @param $activeType
     * @param $hlbl
     * @return array
     */
    public function getTable($userID, $activeType, $hlbl): array
    {
        $res = [];
        $filterArray['UF_USER_ID'] = $userID; //Формируем массив фильтра для выборки значений из HL-блока
        if ($activeType == "ACTIVE")
            $filterArray['UF_ACTIVE'] = 1;

        $hlblock = HighloadBlockTable::getById($hlbl)->fetch();
        $entity = HighloadBlockTable::compileEntity($hlblock); //генерация класса
        $entity_data_class = $entity->getDataClass();
        $rsData = $entity_data_class::getList(array(
            "select" => array("UF_ADDRESS"),
            "filter" => $filterArray
        ));
        while ($arData = $rsData->Fetch()) {
            $res[] = ['data' => $arData];
        }
        return $res;
    }
}