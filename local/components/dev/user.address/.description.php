<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arComponentDescription = array(
    "NAME" => "Адреса пользователя",
    "DESCRIPTION" => "Вывод адресов пользователя",
    "PATH" => array(
        "ID" => "dev_custom",
        "NAME" => "Кастомные компоненты",
    ),
);