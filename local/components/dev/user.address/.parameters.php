<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "ACTIVE" => array(
            "PARENT" => "BASE",
            "NAME" => "Показать все адреса",
            "TYPE" => "LIST",
            "MULTIPLE" => "N",
            "DEFAULT" => "ALL",
            "VALUES" => array(
                "ALL" => "Все адреса",
                "ACTIVE" => "Только активные",
            ),
        ),
    ),
);