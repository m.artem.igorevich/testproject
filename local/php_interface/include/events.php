<?

$eventManager = \Bitrix\Main\EventManager::getInstance();

// event type is <EntityName><EventName>

$eventManager->addEventHandler('', 'UserAddressOnBeforeAdd', "resetCache");
$eventManager->addEventHandler('', 'UserAddressOnBeforeUpdate', "resetCache");
$eventManager->addEventHandler('', 'UserAddressOnBeforeDelete', "resetCache");

function resetCache($fields)
{
    \CBitrixComponent::clearComponentCache("dev:user.address");
}
