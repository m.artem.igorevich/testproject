<?

use Bitrix\Main\Application;

$docRoot = Application::getDocumentRoot();

if (file_exists($docRoot . "/local/php_interface/include/events.php")) {
    require_once $docRoot . "/local/php_interface/include/events.php";
}
